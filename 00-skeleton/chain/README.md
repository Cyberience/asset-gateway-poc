# Ethereum blockchain module

## Setup

```bash
npm init
npm install --save-dev truffle@4.0.1 ethereumjs-testrpc
node_modules/.bin/truffle init
```

* Update Solidity code and tests to the latest version.
* Rename the `development` network in `truffle.js`, so:
  * tests can use the built-in chain
  * manual exploratory tests can be conducted by running
    a separate TestRPC node (using `npm run testrpc`)
* Run tests with `npm test`.
* Run any truffle command with `npm run truffle ...`
* `npm run console` is a shortcut for `truffle console --network testrpc`
