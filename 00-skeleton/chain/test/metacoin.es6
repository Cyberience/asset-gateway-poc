const MetaCoin = artifacts.require("./MetaCoin.sol");

contract('MetaCoin (ES6)', async (accounts) => {

    it("should put 10000 MetaCoin in the first account", async () => {
        const instance = await MetaCoin.deployed();
        const balance = await instance.getBalance.call(accounts[0]);
        assert.equal(balance.valueOf(), 10000, "10000 wasn't in the first account");
    });

    it("should call a function that depends on a linked library", async () => {
        const meta = await MetaCoin.deployed();
        const metaCoinBalance = (await meta.getBalance.call(accounts[0])).toNumber();
        const metaCoinEthBalance = (await meta.getBalanceInEth.call(accounts[0])).toNumber();
        assert.equal(metaCoinEthBalance, 2 * metaCoinBalance,
            "Library function returned unexpected function, linkage may be broken");
    });

    it("should send coin correctly", async () => {
        const meta = await MetaCoin.deployed();
        const account_one = accounts[0];
        const account_two = accounts[1];

        // Get initial balances of first and second account.
        const account_one_starting_balance = (await meta.getBalance.call(account_one)).toNumber();
        const account_two_starting_balance = (await meta.getBalance.call(account_two)).toNumber();
        const amount = 10;

        await meta.sendCoin(account_two, amount, {from: account_one});

        const account_one_ending_balance = (await meta.getBalance.call(account_one)).toNumber();
        const account_two_ending_balance = (await meta.getBalance.call(account_two)).toNumber();

        assert.equal(account_one_ending_balance, account_one_starting_balance - amount,
            "Amount wasn't correctly taken from the sender");

        assert.equal(account_two_ending_balance, account_two_starting_balance + amount,
            "Amount wasn't correctly sent to the receiver");
    });

});
