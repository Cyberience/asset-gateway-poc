#To run on dev env
##Step 1. run testrpc on localhost 8545
```
docker run -d -p 8545:8545 ethereumjs/testrpc:latest
```
##Step 2. go to ../chain
```
truffle compile 
```
and then
```
truffle migrate
 ```
##Step 3. run front end
```
npm run dev
```

##Step 4. hook MetaMask with accounts[0]
The default ethereum account private key can be found in ```docker log CONTAINER_ID_OF_TESTRPC```

#Prerequisites
1. install docker
2. build docker image for testrpc
```
git clone https://github.com/ethereumjs/testrpc.git && cd testrpc && docker build -t ethereumjs/testrpc .
```
3. install truffle@4.0.1
```
npm install -g truffle@4.0.1
```