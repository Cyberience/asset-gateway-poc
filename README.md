# Proof of concept system for OAX asset gateways

## Basic functionality

![](doc/img/asset-gateway-core.png)

## Frontend

Lead: [Sawyer](https://gitlab.com/sawyer.zhang)

It's going to be written using Angular 2, because that's Sawyer's expertise.



## Backend

Lead: [Cerulean](https://gitlab.com/ceruleanhu)

Minimum Nodejs version should be 0.8.6, because it supports ES6 syntax
(eg. async/await and arrow functions)



## Ethereum

Lead: Enuma Technologies

It relies on the [OpenZeppelin](https://github.com/OpenZeppelin/zeppelin-solidity)
libraries.

We hope to have the following benefits from it:

1. it's going to be kept up to date with the evolution of Solidity
2. it will be peer reviewed by both open source and commercial users
   (who are joining the [zeppelinOS](https://zeppelinos.org/) project)
   which should help reducing bugs and security issues
3. it provides the community a standardized vocabulary of frequently used concepts
4. it will allow reducing deployment gas costs
   once an official version gets deployed onto the mainnet
   
It uses [Truffle beta](https://github.com/trufflesuite/truffle/releases/tag/v4.0.0-beta.2)
so, we get access to a convenient development workflow, which:

1. is hopefully getting faster and faster over time
2. keeps up with solidity development
3. allows for reproducible builds
4. provides a common test structure

We also try to stay on the latest Solidity version which is
v0.4.17 at the time of writing, because it is necessary to benefit from
the features coming up in Metropolis.

The developed code should be tested against:

1. the javascript in-memory chain built-in to truffle
2. a [Proof of Authority](http://blog.enuma.io/update/2017/08/29/proof-of-authority-ethereum-networks.html)
   private chain (for speed)
3. Rinkeby (for speed because it's also using PoA, not PoW)
