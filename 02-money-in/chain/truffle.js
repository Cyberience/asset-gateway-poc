// const HDWalletProvider = require("truffle-hdwallet-provider");
//
// const {RINKEBY_MNEMONIC, INFURA_ACCESS_TOKEN} = process.env;
// const rinkebyGas = 4600000;
//
// function newRinkebyWallet(url) {
//     return () => {
//         if (!RINKEBY_MNEMONIC) throw "No RINKEBY_MNEMONIC environment variable specified.";
//         return new HDWalletProvider(RINKEBY_MNEMONIC, url)
//     };
// }

module.exports = {
    networks: {
        // "infura-rinkeby": {
        //     network_id: 4,
        //     gas: rinkebyGas,
        //     provider: newRinkebyWallet('https://rinkeby.infura.io/' + INFURA_ACCESS_TOKEN)
        // },
        // "local-rinkeby": {
        //     network_id: 4,
        //     gas: rinkebyGas,
        //     provider: newRinkebyWallet('http://localhost:8545')
        // }
    },
    mocha: {
        slow: 1000
    }
};
