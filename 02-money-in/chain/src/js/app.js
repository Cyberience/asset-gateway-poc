App = {
  web3Provider: null,
  contracts: {},
  accountsData: {},

  init: async() => {
    await App.initWeb3();
    data = await $.getJSON('../accounts.json');
    accountsData = data;
    var row = $('#row');
    var template = $('#template');

    for (i = 0; i < data.length; i++) {
      template.find('.panel-title').text(data[i].name);
      template.find('.panel-body').attr('id', data[i].id);
      template.find('.account').text(data[i].id);
      template.find('.ethAccount').text(data[i].ethAccount);
      template.find('.balance').text(await App.getBalance(data[i].ethAccount));
      template.find('.currency').text(data[i].currency);

      if ( 0 == i ) {
        template.find('.btn-burn').show() 
      } else { 
        template.find('.btn-burn').hide() 
      }

      row.append(template.html());
    }

  },

  initWeb3: async() => {
    // Is there is an injected web3 instance?
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
    } else {
      // If no injected web3 instance is detected, fallback to the TestRPC
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
    }
    web3 = new Web3(App.web3Provider);

    return App.initContract();
  },

  initContract: async() => {
    data = await $.getJSON('Gate.json')
      // Get the necessary contract artifact file and instantiate it with truffle-contract
    var dataArtifact = data;
    App.contracts.Gate = TruffleContract(dataArtifact);

    // Set the provider for our contract
    App.contracts.Gate.setProvider(App.web3Provider);

    return App.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '.btn-mint', App.handleMint);
    $(document).on('click', '.btn-burn', App.handleBurn);
  },

  //Interact with the contract, use as example to do Mint and Burn, and get the totals.
  getBalance: async(ethAccount) => {
      var iBalance = await App.contracts.Gate.deployed(); // Ensure the Contract is deployed

      retVal = await iBalance.balanceOf.call(ethAccount).valueOf();
      console.log("Returnd Values for:",ethAccount,retVal);
      return retVal;
  },

  getAccounts: () => {
    return new Promise( (resolve,reject) => {
      web3.eth.getAccounts(function(error, accounts) {
        if (error) {
          return reject(error);
        } else if (accounts.length == 0) {
          alert('Please log into Metamask account before proceeding');
        } else {
          resolve(accounts);
        }
      });
    });
  },

  handleMint: async() => {
    event.preventDefault();
    var targetParent = $(event.target).parent(); // Get the parent for the account ID
    var acId = await targetParent.get(0).id; // Get the account ID
    var ethId = accountsData[acId].ethAccount;
    var amount = targetParent.children('.input-mint').val();
    targetParent.children('.input-mint').val('') ; // Reset input
    targetParent.children('.timeout').show() ; // Update the Balance

    console.log("Minting",amount,"to",ethId);

    try {
      var iMint = await App.contracts.Gate.deployed(); 
      await iMint.mint(ethId,amount);
    } catch(err) {
      console.log("During the Minting Call to contract",err);
    } finally { // Delay due to Blockchain update time.
      setTimeout(function(){
        App.updateBalance(targetParent);
      },2000);
    }
  },

  handleBurn: async(_amount) => { //Can only Burn an owned account
    event.preventDefault();
    var targetParent = $(event.target).parent(); // Get the parent for the account ID
    var acId = await targetParent.get(0).id; //Get the account Id from the UI
    var ethId = accountsData[acId].ethAccount;
    var amount = targetParent.children('.input-mint').val();
    console.log("Burning",amount,"from",ethId);
    targetParent.children('.input-mint').val('') ; //reset input
    targetParent.children('.timeout').show() ; //update the Balance

    try {
      var iBurn = await App.contracts.Gate.deployed(); 
      await g.burn(amount);
    } catch(err) {
      console.log("During the Burning Call to contract",err);
    } finally {
      setTimeout(function(){ // Delay due to Blockchain update time.
        App.updateBalance(targetParent);
      },2000);
    }
  },

  debug: async(_info) => {
    // TODO use public accessor functionality
    // console.log(_info,"Owner",await iMint.whoIsTheOwner(),"Sender",await iMint.whoIsTheSender() );
  },

  updateBalance: async(_targetParent) => {
    acId = await _targetParent.get(0).id; //Get the account Id from the UI
    try {
      var myAccounts = await App.getAccounts();
      var ethId = accountsData[acId].ethAccount;
      var newBalance = await App.getBalance(ethId);
    } catch(e) {
      console.log("Getting Balance",e);
    }
    _targetParent.children('.balance').text(newBalance.toString() ) ; //update the Balance
    _targetParent.children('.timeout').hide() ; //update the Balance
  }

};

$(function() {
  $(window).load(function() {
    App.init();
  });
});