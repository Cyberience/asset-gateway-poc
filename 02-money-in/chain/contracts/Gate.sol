pragma solidity ^0.4.16;


import "zeppelin-solidity/contracts/token/BurnableToken.sol";
import "zeppelin-solidity/contracts/token/MintableToken.sol";


contract Gate is BurnableToken, MintableToken {
    int constant typeOwner = 1; // Original Assigned deployer
    int constant typeOp = 2; // All operators with power to control the contract (To be congress)
    uint8 public opCount = 0;

    mapping (address => uint256) public status;
    mapping (address => int) public owners;

    event OwnerAdded(address _user);
    event OwnerRemoved(address _user);
    event Minted(address indexed to, uint256 amount, bytes txCode);


    /**
    * Constructor: Gate
    * Purpose: Adds the deployer to the Owner list
    */
    function Gate() public { 
        owners[msg.sender] = typeOwner; 
    }

    /**
    * Modifier: onlyOwner
    * Purpose: To over ride the ownership and introduce a group of owners
    * Note: Override the ERC20 owner to a owner group defined as owners
    */
    modifier onlyOwner() {
        require(owners[msg.sender] == typeOp || owners[msg.sender] == typeOwner); 
        _;
    }

    /**
    * Function: addOwner
    * Purpose: Add an address to the owner mapping
    * Note: Group of owners, exist, this is to remove one, but the last one can not be removed, this would lead to a dead contract, where only the deployer can recover, but the deployer can be removed
    */
    function addOperator(address _newOp)  onlyOwner public {
        require(owners[_newOp] > 0 ); // Ensure address is not already listed or blocked
        owners[_newOp] = typeOp; 
        opCount++;
        OwnerAdded(_newOp);
    }

    /**
    * Function: removeOwner
    * Purpose: Remove an address from the owner list
    * Note: Group of owners, exist, removing is tricky, you must not remove the last Operator, as you may kill the contract.
    */
    function removeOperator(address _oldOp)  onlyOwner public {
        require(opCount > 1);
        delete owners[_oldOp]; 
        owners[_oldOp] = typeOp; 
        opCount--;
        OwnerRemoved(_oldOp);
    }

    /**
    * Function: mint
    * Purpose: Override the mint function to include transaction tracking
    */
    function mint(address _to, uint256 _amount, bytes _txCode) onlyOwner canMint public returns (bool) {
        mint(_to, _amount);
        Minted(_to, _amount, _txCode);
    
        return true;
    }

}