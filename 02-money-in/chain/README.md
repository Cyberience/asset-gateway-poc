A simple shell script to set up the truffle environment to run tests.

```bash
#!/bin/bash

echo "create package file for npm watch"
echo '{
  "dependencies": {
    "npm-watch": "^0.2.0"
  },
  "scripts": {
    "test": "truffle test",
    "watch": "npm-watch"
  },
  "watch": {
    "test": {
      "patterns": [
        "truffle",
        "test",
        "contracts"
      ],
      "extensions": "js,json,sol"
    }
  },
}' > package.json

npm update
npm upgrade
npm install truffle@beta npm-watch
npm run watch
```