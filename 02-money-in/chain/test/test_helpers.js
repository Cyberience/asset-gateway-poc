const assert = require('assert');

async function assertThrowsAsync(fn, regExp, message) {
    let f = () => {};
    try {
        await fn();
    } catch(e) {
        f = () => {throw e};
    } finally {
        assert.throws(f, regExp, message);
    }
}

async function assertContractThrows(fn, regExp, txt) {
    await assertThrowsAsync(fn, /invalid opcode/, 'Contract call should fail')
}

async function assertBalance(contract, address, expected, message) {
    assert.equal((await contract.balanceOf.call(address)).toNumber(), expected, message)
}

module.exports = {
    assertThrowsAsync: assertThrowsAsync,
    assertContractThrows: assertContractThrows,
    assertBalance: assertBalance
}
