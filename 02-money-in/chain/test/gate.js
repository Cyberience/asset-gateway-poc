const Gate = artifacts.require('Gate');
const {assertContractThrows, assertBalance} = require('./test_helpers')

contract('Gate', ([deployer, customer1, customer2]) => {
    let g;

    beforeEach(async () => g = await Gate.new())

    it("can mint", async () => {
        await g.mint(deployer, 50);
        await assertBalance(g, deployer, 50);
    });

    it("can burn", async () => {
        await g.mint(deployer, 100);
        await g.burn(10);
        await assertBalance(g, deployer, 90);
    });

    it("can not burn more than available supply", async () => {
        await g.mint(deployer, 100);
        await assertContractThrows(async () =>
            await g.burn(200))
    });

    it("can transfer", async () => {
        await g.mint(deployer, 10);
        await g.transfer(customer1, 1);
        await assertBalance(g, deployer, 9);
        await assertBalance(g, customer1, 1);

        await g.transfer(customer2, 2);
        await assertBalance(g, deployer, 7);
        await assertBalance(g, customer2, 2);
    });

    it("can transfer if approved", async () => {
        await g.mint(deployer, 10);
        await g.approve(customer1, 3, {from: deployer})
        await g.transferFrom(deployer, customer1, 1, {from: customer1})
        await g.transferFrom(deployer, customer1, 2, {from: customer1})
        await assertBalance(g, customer1, 3);
        await assertBalance(g, deployer, 7);
    });

    it("can not transfer without approval", async () => {
        await g.mint(deployer, 10);
        await assertContractThrows(async () =>
            await g.transferFrom(deployer, customer1, 1, {from: customer1}))
    });
});