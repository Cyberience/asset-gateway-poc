import { Injectable } from '@angular/core';
// import {LocalStorage} from 'ngx-webstorage';
import {User} from '../models/user';

@Injectable()
export class AuthService{
  isLoggedIn: boolean = false;
  redirectUrl: string = '';

  // @LocalStorage('user')
  user: User;
  constructor(){
    this.user = new User();
    this.user.loadForLS();

    if(this.user.hasOwnProperty('id')){
      this.isLoggedIn = true;
    } else {
      this.user.id = '001',
      this.user.name = 'anxing';
      this.user.save();
      console.log('login successed');
    }


    console.log('user', this.user);
  }


}
