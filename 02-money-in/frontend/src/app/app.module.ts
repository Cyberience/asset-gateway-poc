import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import { AppComponent } from './app.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {AppRoutingModule} from './app-routing.module';
import {MdButtonModule, MdCardModule, MdDialogModule, MdGridListModule, MdInputModule, MdRadioModule, MdMenuModule, MdSelectModule, MdTableModule, MdToolbarModule, MdIconModule} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HomeComponent} from './home/home.component';
import { GatewayDetailComponent } from './gateway/gateway-detail/gateway-detail.component';
import { GatewayDashboardComponent } from './gateway/gateway-dashboard/gateway-dashboard.component';
import { GatewayDepositComponent } from './gateway/gateway-deposit/gateway-deposit.component';
import { GatewayMintComponent } from './gateway/gateway-mint/gateway-mint.component';
import { GatewaySubmitComponent } from './gateway/gateway-submit/gateway-submit.component';
import {AuthService} from './services/auth.service';
import {AuthGuard} from './services/auth-guard.service';
import {LoginComponent} from './user/login/login.component';
import {RegisterComponent} from './user/register/register.component';
import { OverviewComponent } from './admin/overview/overview.component';
import { VerifyRequestComponent } from './admin/verify-request/verify-request.component';
import { MintTokenComponent } from './admin/mint-token/mint-token.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, 'i18n/', '.json');
}


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GatewayDetailComponent,
    GatewayDashboardComponent,
    RegisterComponent,
    LoginComponent,
    GatewayDepositComponent,
    GatewayMintComponent,
    GatewaySubmitComponent,
    OverviewComponent,
    VerifyRequestComponent,
    MintTokenComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AppRoutingModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    NgxDatatableModule,

    MdSelectModule,
    MdTableModule,
    MdInputModule,
    MdButtonModule,
    MdMenuModule,
    MdDialogModule,
    MdCardModule,
    MdGridListModule,
    MdToolbarModule,
    MdIconModule,
    MdRadioModule,


  ],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
