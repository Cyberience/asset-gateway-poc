import {Injectable} from '@angular/core';
import Web3 from 'web3';
import {default as TruffleContract} from 'truffle-contract';
import gatecoin_artifacts from '../../../build/contracts/Gate.json';
import {Subject,Observable} from 'rxjs/Rx';

declare let window: any;

@Injectable()
export class Web3Service {
  private _name = 'Web3.Service';
  private web3: Web3;
  private contract;

  private accounts: string[];
  public ready = false;
  public accountsObservable = new Subject<string[]>();

  constructor() {
    window.addEventListener('load', (event) => {
      this.bootstrapWeb3();
    });
  }

  public bootstrapWeb3() {

    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof window.web3 !== 'undefined') {
      // Use Mist/MetaMask's provider
      this.web3 = new Web3(window.web3.currentProvider);
    } else {
      console.log('No web3? You should consider trying MetaMask!');

      // Hack to provide backwards compatibility for Truffle, which uses web3js 0.20.x
      Web3.providers.HttpProvider.prototype.sendAsync = Web3.providers.HttpProvider.prototype.send;
      // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
      this.web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
    }

    this.initContract()

    //updating account every 1 sec
    Observable
      .interval(1000)
      .flatMap(i=> Observable.bindNodeCallback(this.web3.eth.getAccounts)())
      .subscribe(
        (accounts:string[])=>{
          if (accounts.length === 0) {
            console.error(`${this._name}: Couldn't get any accounts!`)
            return;
          }

          if (!this.accounts || this.accounts.length !== accounts.length || this.accounts[0] !== accounts[0]) {
            console.log('Observed new accounts');
            this.accountsObservable.next(accounts);
            this.accounts = accounts;
          }

        },
        (error)=>console.error(`${this._name}: There is an error updating accounts. ${error}`)
      );
  }

  initContract(){
    this.contract = TruffleContract(gatecoin_artifacts);
    this.contract.setProvider(this.web3.currentProvider);
  }

}
