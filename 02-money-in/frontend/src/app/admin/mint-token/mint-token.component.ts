import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mint-token',
  templateUrl: './mint-token.component.html',
  styleUrls: ['./mint-token.component.scss']
})
export class MintTokenComponent implements OnInit {
  verifyStatus: number = 0;
  mintingFlag = false;

  actions= [{
    name: 'Mint',
  },{
    name: 'Redemption',
  },{
    name: 'Amount',
  }];

  constructor() { }

  ngOnInit() {
  }

  mint(){
    this.mintingFlag = true;
    setTimeout(() => {
      this.verifyStatus = 1;
      this.mintingFlag = false;
    }, 2000);
  }

}
