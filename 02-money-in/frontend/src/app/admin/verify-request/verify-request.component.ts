import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-verify-request',
  templateUrl: './verify-request.component.html',
  styleUrls: ['./verify-request.component.scss']
})
export class VerifyRequestComponent implements OnInit {

  verifyStatus: number = 0;

  actions= [{
    name: 'Mint',
  },{
    name: 'Redeeption',
  },{
    name: 'Amount',
  }];

  rows = [{
    no: '001',
    type: 'Mint',
    userId: '0891231883839',
    ccy: 'USD',
    amount: '127287383.45',
    date: '10 Oct 2017',
    status: 1,
    action: 1,
  }];

  constructor() { }

  ngOnInit() {
  }

  verify(){
    setTimeout(() => {
      this.verifyStatus = 1;
    }, 2000);
  }

}
