import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-gateway-mint',
  templateUrl: './gateway-mint.component.html',
  styleUrls: ['./gateway-mint.component.scss']
})
export class GatewayMintComponent implements OnInit {

  layouts = [
    {text: 'One', cols: 4, rows: 4, color: 'lightblue'},
    {text: 'Two', cols: 12, rows: 12, color: 'lightgreen'},
    {text: 'Three', cols: 4, rows: 8, color: 'lightpink'},
  ];

  tokenLists = [
    {img: '', name: 'USDT', amount: 1000},
    {img: '', name: 'HKDT', amount: 1000},
  ]

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
  }

}
