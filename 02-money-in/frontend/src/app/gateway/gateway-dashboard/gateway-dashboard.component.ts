import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-gateway-dashboard',
  templateUrl: './gateway-dashboard.component.html',
  styleUrls: ['./gateway-dashboard.component.scss']
})
export class GatewayDashboardComponent implements OnInit {

  layouts = [
    {text: 'One', cols: 4, rows: 4, color: 'lightblue'},
    {text: 'Two', cols: 12, rows: 12, color: 'lightgreen'},
    {text: 'Three', cols: 4, rows: 8, color: 'lightpink'},
  ];

  tokenLists = [
    {img: '', name: 'USDT', amount: 1000},
    {img: '', name: 'HKDT', amount: 1000},
  ]

  records = [
    {
      no: 'xxxxxxxxx01',
      date: '2017-11-09 10:23:45',
      status: 'SUBMITED',
      currency: 'USD',
      token: 'USD',
      amount: 1000,
      code: 'X50Y930D',
      bank: 'BOCHK xxxxxxxxxx'
    },{
      no: 'xxxxxxxxx02',
      date: '2017-11-10 15:53:45',
      status: 'MINT',
      currency: 'USD',
      token: 'USD',
      amount: 2000,
      code: 'X5A8Y930D',
      bank: 'BOCHK xxxxxxxxxx'
    }

  ];

  selectIndex: string;

  radioValue: string;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
  }

  recordClick(iIndex){
    this.selectIndex = iIndex;
    this.radioValue = 'md-radio-' + iIndex;
  }

}
