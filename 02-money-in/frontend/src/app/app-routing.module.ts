import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LocalizeRouterModule, LocalizeRouterSettings, LocalizeParser } from 'localize-router';
import { LocalizeRouterHttpLoader } from 'localize-router-http-loader';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import {HomeComponent} from './home/home.component';
import {GatewayDetailComponent} from './gateway/gateway-detail/gateway-detail.component';
import {GatewayDashboardComponent} from './gateway/gateway-dashboard/gateway-dashboard.component';
import { GatewayDepositComponent} from './gateway/gateway-deposit/gateway-deposit.component';
import { GatewayMintComponent } from './gateway/gateway-mint/gateway-mint.component';
import { GatewaySubmitComponent } from './gateway/gateway-submit/gateway-submit.component';
import {LoginComponent} from './user/login/login.component';
import {RegisterComponent} from './user/register/register.component';

import {OverviewComponent} from './admin/overview/overview.component';
import {VerifyRequestComponent} from './admin/verify-request/verify-request.component';
import {MintTokenComponent} from './admin/mint-token/mint-token.component';

export function HttpLoaderFactory(translate: TranslateService, location: Location, settings: LocalizeRouterSettings, http: HttpClient) {
  return new LocalizeRouterHttpLoader(translate, location, settings, http, 'i18n/locales.json');
}

const routes = [
  { path: '', component: HomeComponent },
  { path: 'gateway/detail', component: GatewayDetailComponent },
  {
    path: 'gateway/dashboard',
    component: GatewayDashboardComponent,
    children: [{
      path: '',
      component: GatewayDashboardComponent,
    }]
  },
  {
    path: 'gateway/dashboard/deposit',
    component: GatewayDepositComponent,
  },{
    path: 'gateway/dashboard/mint',
    component: GatewayMintComponent,
  },{
    path: 'gateway/dashboard/submit',
    component: GatewaySubmitComponent,
  },{
    path: 'admin/overview',
    component: OverviewComponent,
  },{
    path: 'admin/verify-request',
    component: VerifyRequestComponent,
  },{
    path: 'admin/mint-token',
    component: MintTokenComponent,
  },
  { path: 'account/register', component: RegisterComponent},
  { path: 'account/login', component: LoginComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    LocalizeRouterModule.forRoot(routes, {
      parser: {
        provide: LocalizeParser,
        useFactory: HttpLoaderFactory,
        deps: [TranslateService, Location, LocalizeRouterSettings, HttpClient]
      }
    })
  ],
  exports: [ RouterModule, LocalizeRouterModule ]
})
export class AppRoutingModule {}
