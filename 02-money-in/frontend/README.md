# Frontend

An Angular 2 frontend for both a RESTful API and an Ethereum network (through Metamask).

# Global dependencies

```bash
npm install -g yarn
```

# Development

```bash
yarn install
npm start
open http://localhost:4200/
```
