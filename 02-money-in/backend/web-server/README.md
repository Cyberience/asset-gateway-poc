#This application includes
- APIs servicing frontend
- Web sockets/APIs to listen to blockchain-integration
- Traditional DB to persist business flow/logic
- Object Relationship Mapping (ORM)
- Transactional DB write

#To start backend app in dev env
##Prerequisites
###Step 1. Install mysql following guide [here](https://dev.mysql.com/doc/mysql-osx-excerpt/5.7/en/osx-installation-pkg.html)
###Step 2. Download mysql workbench [here](https://dev.mysql.com/downloads/workbench/) and install it
###Step 3. Create database oax_backend in mysql instance
```create database oax_backend;```

##During development
###Step 4. Install dependencies
```npm install```
###Step 5. Start app
```.../backend$ node app.js```

Site is now up at localhost:5000
