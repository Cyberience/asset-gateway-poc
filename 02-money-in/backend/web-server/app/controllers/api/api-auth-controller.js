var exports = module.exports = {}

exports.signUp = function (req, res) {

    let exampleUser = {
        email: {
            type: "string",
            format: "email",
            minLength: 4,
            maxLength: 255,
            placeHolder: "example.user.email.text"},
        firstname: {
            type: "string",
            minLength: 1,
            maxLength: 255,
            placeHolder: "example.user.firstname.text"},
        lastname: {
            type: "string",
            minLength: 1,
            maxLength: 255,
            placeHolder: "example.user.lastname.text"},
        password: {
            type: "string",
            format: "password",
            minLength: 8,
            maxLength: 255,
            placeHolder: "example.user.password.text"}
    }

    res.end(JSON.stringify(exampleUser));

}