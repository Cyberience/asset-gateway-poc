var apiAuthController = require('../controllers/api/api-auth-controller.js');
let apiDefaultPath = "/api/v1";
module.exports = function (app, passport) {
    app.get(apiDefaultPath+'/user/signup', apiAuthController.signUp);
    app.post(apiDefaultPath+'/user/signUp', passport.authenticate('local-signup', {
            successRedirect: '/dashboard',

            failureRedirect: '/user/signUp'
        }
    ));
}