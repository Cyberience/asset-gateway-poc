swagger: "2.0"
info:
  description: "Proof of concept of an asset gateway to join OAX network. A user can submit KYC documents and then mint, transfer, burn fiat token via this asset gateway."
  version: "1.0.0"
  title: "OAX Asset Gateway POC"
  termsOfService: ""
  contact:
    email: "projectstallman@openanx.org"
  license:
    name: "Apache 2.0"
    url: "https://www.apache.org/licenses/LICENSE-2.0.html"
host: "localhost:5000"
basePath: "/api/v1"
tags:
- name: "user"
- name: "operator"
schemes:
- "http"
paths:
  /user/signUp:
    post:
      tags:
      - "user"
      summary: "Sign up new user"
      operationId: "signUpUser"
      produces:
      - "application/vnd.api+json"
      parameters:
      - in: "body"
        name: "body"
        description: "User Sign Up Info"
        required: true
        schema:
          $ref: "#/definitions/User"
      responses:
        201:
          description: "Successfully sign up user"
          schema:
            $ref: "#/definitions/UserInfo"
        400:
          description: "Input fields doesn't pass validation check"
          schema:
            $ref: "#/definitions/Errors"
  /user/signIn:
    get:
      tags:
      - "user"
      summary: "Sign in existing user"
      operationId: "signInUser"
      produces:
      - "application/vnd.api+json"
      parameters:
      - name: "email"
        in: "query"
        description: "The user email for login"
        required: true
        type: "string"
        format: email
      - name: "password"
        in: "query"
        description: "The password for login in clear text"
        required: true
        type: "string"
        format: password
      responses:
        200:
          description: "Successfully sign in user"
        400:
          description: "Input fields doesn't pass validation check"
          schema:
            $ref: "#/definitions/Errors"
        401:
          description: "Sign in failed with wrong combination of email+password"
          schema:
            $ref: "#/definitions/Errors"
        403:
          description: "User is blocked from using the site"
          schema:
            $ref: "#/definitions/Errors"
  /mintRequests:
    get:
      tags:
      - "user"
      - "operator"
      description: If the logged in user is an operator, this endpoint returns all mint requests stored in the system; if not, only return what's submitted by this user.
      responses:
        200:
          description: OK
          schema:
            $ref: "#/definitions/StoredMintRequests"
    post:
      tags:
      - "user"
      summary: "User submit mint request"
      operationId: "submitMintRequest"
      produces:
      - "application/vnd.api+json"
      parameters:
      - in: "body"
        name: "body"
        description: "User Submit Mint Request"
        required: true
        schema:
          $ref: "#/definitions/MintRequest"
      responses:
        201:
          description: "Successfully submitted mint request"
          schema:
            $ref: "#/definitions/MintRequestResponse"
        400:
          description: "Input fields doesn't pass validation check"
          schema:
            $ref: "#/definitions/Errors"
        403:
          description: "if the user is not logged in"
          schema:
            $ref: "#/definitions/Errors"
  /mintRequests/{internal_transaction_id}/verify:
    put:
      description: Operator trying to verify a deposit and mint after reverification;
      tags:
      - "operator"
      operationId: "verifyMintRequest"
      produces:
      - "application/vnd.api+json"
      parameters:
      - in: "path"
        name: "internal_transaction_id"
        description: "Internal Txn Id for the mining request"
        required: true
        type: string
      - in: "body"
        name: "body"
        description: "Operator Verify Mint Request"
        required: true
        schema:
          $ref: "#/definitions/MintVerificationRequest"
      responses:
          200:
            description: "Successfully verified mint request"
            schema:
              $ref: "#/definitions/MintVerificationRequestResponse"
          400:
            description: "If the deposit code does not match"
  /mintRequests/{internal_transaction_id}/execute:
    put:
      description: Operator trying to fire a mint action on the blockchain;
      tags:
      - "operator"
      operationId: "executeMintRequest"
      produces:
      - "application/vnd.api+json"
      parameters:
      - in: "path"
        name: "internal_transaction_id"
        description: "Internal Txn Id for the mining request"
        required: true
        type: string
      responses:
          204:
            description: "Successfully acknowledge the execution but do nothing than changing the state from FIAT_TRANSFERRED to MINTING"
definitions:
  User:
    type: object
    required: ["data"]
    properties:
      data:
        type: object
        properties:
          type:
            type: string
            example: "user"
          attributes:
            type: object
            required: ["email", "firstName", "lastName", "password"]
            properties:
              email:
                type: string
                format: email
              firstName:
                type: string
              lastName:
                type: string
              password:
                type: string
                format: password
  UserInfo:
    type: object
    required: ["data"]
    properties:
      data:
        type: object
        properties:
          type:
            type: string
            example: "user"
          attributes:
            type: object
            required: ["email", "firstName", "lastName"]
            properties:
              email:
                type: string
                format: email
              firstName:
                type: string
              lastName:
                type: string
  MintRequest:
    type: object
    required: ["data"]
    properties:
      data:
        type: object
        properties:
          type:
            type: string
            example: "mintRequests"
          attributes:
            type: object
            required: ["depositMethod","currency","amount","blockchainAddress"]
            properties:
              depositMethod:
                enum: ["BANK_WIRE"]
              currency:
                enum: ["USD"]
                description: Use enum because it's official fiat at this stage
              amount:
                type: number
              blockchainAddress:
                type: string
                description: For now, only ether address is supported
  StoredMintRequests:
    type: object
    required: ["data"]
    properties:
      data:
        type: array
        items:
          type: object
          properties:
            type:
              type: string
              example: "mintRequests"
            attributes:
              type: object
              required: ["depositMethod","currency","amount","blockchainAddress"]
              properties:
                internalTransactionId:
                  type: string
                userId:
                  description: system generated user id
                  type: string
                currency:
                  enum: ["USD"]
                  description: Use enum because it's official fiat at this stage
                amount:
                  type: number
                dateCreated:
                  type: integer
                  description: time in unix timestamp millisec
                status:
                  enum: ["SUBMITTED","FIAT_TRANSFERRED","TOKEN_MINTED","REJECTED"]
                depositMethod:
                  enum: ["BANK_WIRE"]
                blockchainAddress:
                  type: string
                  description: For now, only ether address is supported
  MintRequestResponse:
    type: object
    required: ["data"]
    properties:
      data:
        type: object
        properties:
          type:
            type: string
            example: "mintRequests"
          attributes:
            type: object
            required: ["depositMethod","currency","amount","status","depositCode"]
            properties:
              depositMethod:
                enum: ["BANK_WIRE"]
              currency:
                enum: ["USD"]
              amount:
                type: number
              status:
                enum: ["SUBMITTED","FIAT_TRANSFERRED","TOKEN_MINTED","REJECTED"]
                description: It can only be either SUBMITTED or REJECTED at this stage; users who are not KYC verified will be rejected to submit such requst;
                example: "SUBMITTED"
              depositCode:
                type: string
                description: Unique code generated by backend to distinguish this mint request
                example: Y52SEW8
  MintVerificationRequest:
    type: object
    required: ["data"]
    properties:
      data:
        type: object
        properties:
          type:
            type: string
            example: "mintRequests"
          attributes:
            type: object
            required: ["depositCode","referenceTransactionId"]
            properties:
              referenceTransactionId:
                type: string
              depositCode:
                type: string
  MintVerificationRequestResponse:
    type: object
    required: ["data"]
    properties:
      data:
        type: object
        properties:
          type:
            type: string
            example: "mintRequests"
          attributes:
            type: object
            required: ["status"]
            properties:
              status:
                enum: ["SUBMITTED","FIAT_TRANSFERRED","TOKEN_MINTED","REJECTED"]
                description: it can only be FIAT_TRANSFERRED at this stage
                example: "FIAT_TRANSFERRED"
  Errors:
    type: object
    properties:
      errors:
        type: array
        items:
          $ref: "#/definitions/Error"
  Error:
    type: object
    required: ["title"]
    properties:
      id:
        type: string
      links:
        type: object
        properties:
          about:
            type: string
            example: "https://oax.org/asset-gateway/api/v1/error/000001"
      status:
        type: string
        example: "400"
      code:
        type: string
        example: 'oax.error.000001'
      title:
        type: string
        example: "error.email.invalid.format"
      detail:
        type: string
        example: 'error.email.invalid.format.detail'
      source:
        type: object
        properties:
          pointer:
            type: string
            example: "/data/attributes/email"
          parameter:
            type: string
            example: "email"
      meta:
        $ref: "#/definitions/Meta"
  Meta:
    type: object
externalDocs:
  description: "Find out more about OAX"
  url: "https://oax.org"