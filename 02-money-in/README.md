# 02-money-in stage
Money In Should include Mint and Burn function for the Gateway Level.

The enclosed folder called Exchange has contract that uses Truffle to import the ERC20 Token Standard, and a Node Web ui that represents Min and Burn.
Dependancies are
* Truffle - Beta
* Testrpc
* Node
* Metamask

To run this project you will need to do the follow:
Navigate to the Exchange Folder
```
cd chain 
```
Update the Node Packages
```
npm i
```
This is now Truffle 4 compatible, gas needed to be added to the truffle.js file to comply with new requirements of truffle test

Install the test RPC
```
npm i -g ethereumjs-testrpc
```
Start up testRPC with the -m paramter to allow the default accounts to work.
```
testrpc -m "awkward injury hospital pond scene better birth aware lounge pluck sustain wink"
```
and this will generate a list of accounts as shown below
```
Available Accounts
==================
(0) 0xfc7f953753d782222b74d5cf859796046a8c7884
(1) 0xdd7e4dfd176bee8a4581e171807bee01a6e55bab
(2) 0xaea0f38274023ba717ecccab331ab4edbc2ab908
(3) 0x9e28e7f28ecea227a3b3f9157690a650aea3eda8
(4) 0xba94ae9eff78cb5054d54eebdd058b2a59220f20
(5) 0x99d2c463624608103db6db30c65dd8017372aa8d
(6) 0xa155b72d29073065d64625e3d6e2e41266cf689f
(7) 0x80b6f7e49f5c1ce117dd7090002f7f1b91e39116
(8) 0x3eea53c0144177ebdd87269c91f419fb82fea2d6
(9) 0x28053b1123fc3244dc6163710aeda9dc5997c783

Private Keys
==================
(0) 7bb244a4bf91b5741e566daac6baa1fa5d76a42d7858df97d120a4c9ecebd1f3
(1) 9ef2a601b1825df0ff4ef5092523530fa085b3cf850ea362522161dc9dd65a0e
(2) 482c4d37af2702c4e3094b6f9f16efd969b355b2520d93df70d074c5917b8844
(3) bc6bdf326fe59de02f8f257ed0dfe1a7fdb1f47e1a23e4d0b8322837ada64814
(4) 2b26a1c87bb07374cf55a61d93ae4a98340f09e77c4563ca49f7e65154b4ac83
(5) 79c0b14e00017c93417ea830b5e01902105e5f5c8d426cd9abdc857b4aceea8d
(6) c1eb8a4b8c14ca36211b91a10abffbb0f5a0132ac0e7baa201a73b0c46001b38
(7) b0726d1c16ea8306f382191a77b881f51079966b4d74e5a5c7e04b7109ef16de
(8) 3c8616f804d7d91897a6fdb88915b33cbb2d53c53936d8f284129e619c5aab5f
(9) 2e96e2786514a0c0fed25013a9ce7047ed1d12a4a20ea7edab0087dbe21fc7e3

HD Wallet
==================
Mnemonic:      awkward injury hospital pond scene better birth aware lounge pluck sustain wink
```
And will be set up as: ```Listening on localhost:8545```
Open up another Terminal window ```Command-T``` on a mac. and from the same Exchange folder execute the following command:
```npm run watch```
This will start the node test scripts that will run every time you change and script or javascript code. a good result should look like the following:
```
[test]   Contract: Gate
[test]     ✓ The Owner is 0xfc7f953753d782222b74d5cf859796046a8c7884
[test]     ✓ The Caller in 0xfc7f953753d782222b74d5cf859796046a8c7884
[test]     ✓ Lets Mint 50 Tokens (72ms)
[test] Remaining Balance is 320
[test]     ✓ Testing Minting (260ms)
[test] Remaining Balance is 60
[test]     ✓ Testing Burning (195ms)
[test] 
[test] 
[test]   5 passing (634ms)
[test] 
[test] [nodemon] clean exit - waiting for changes before restart
```
With all this running, you can start the front end to test mint and burn. to do this. open up a 3rd Terminal ```command-T``` and run the node application, from the same folder type:
```
npm run ui
```
For those interested, the command to run in this case ui is configured within the package.json file that node package manager uses to know what modules to load, install and in this case execute.

Moving forward, you can now do mint and burn. the only catch is, you will need to log into metamask, and will need to import the accounts created by testrpc. once you log in, and select the account, you can do mint and burn. you should also be able to do transfer as the contracts are already ERC20 enabled, but for this particular Phase, no tests scripts have been established, and refacturing of the underlining code is ready for member voting and updates.
