# 01-fiat-token stage

This stage just shows the most minimal setup necessary for
a generic ERC20 token using already audited code from the
[OpenZeppelin](https://github.com/OpenZeppelin/zeppelin-solidity) 
library, so it can be used as a sandbox for discussions and experiments.

There are already automated tests for its functionality, which can
be used as a starting point for further work:

https://github.com/OpenZeppelin/zeppelin-solidity/blob/master/test/StandardToken.js

This stage also demonstrates the basics of a complete Dapp architecture
we are going to build upon.

A fluent development workflow can speed up progress.
Having a minimal but complete environment allows proposing/discussing
changes, improvements to the workflow, which can be tested quickly
and with the least chance of interference with the specifics of the app. 


## Usage

Test smart contracts continuously:
```
cd chain
npm run watch
```

Start a manual dev env:

```
npx testrpc
npx truffle compile 
npx truffle migrate
```
